<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<jsp:useBean id="Scenarios" class="dto.scenarioservice.entities.ScenarioListDto" scope="request"/>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="resources/css/style.css">
    <script type="text/javascript" src="/resources/js/app.js"></script>
    <title>SmartHome</title>
</head>
<body>
<jsp:include page="top1.jsp"></jsp:include>

<h1>scenarios page</h1>

<table class="table" border="1">
    <thead>
    <tr>
        <th>Название сценария</th>
        <th>Устройство</th>
        <th>Комманда</th>
        <th>Статус</th>
        <th>Время выполнения</th>
        <th>UserId</th>
        <th>Действия(просмотр, ред., удаление)</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="item" items="${Scenarios}">
        <tr>
            <td scope="row">${item.aliasName}</td>
            <td>${item.deviceId}</td>
            <td>${item.commandId}</td>
            <td>${item.status}</td>
            <td>${item.runTime}</td>v
            <td>${item.userId}</td>
             <td>
                 <a href="${pageContext.request.contextPath}/web/scenarioView/?id=${item.id}">View</a>
                 <a href="${pageContext.request.contextPath}/web/scenarioEdit/?id=${item.id}">Edit</a>
                 <a href="http://localhost:8762/api/device/devices/device/delete/?id=${item.id}">Delete</a>
             </td>
        </tr>
    </c:forEach>
    </tbody>
</table>

</body>
</html>