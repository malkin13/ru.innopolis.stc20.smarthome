<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<jsp:useBean id="Devs" class="dto.deviceservice.entities.DeviceListDto" scope="request"/>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="resources/css/style.css">
    <script type="text/javascript" src="/resources/js/app.js"></script>
    <title>SmartHome</title>
</head>
<body>

<jsp:include page="top1.jsp"></jsp:include>

<%--<h1>device page</h1>--%>
<%--<table class="table" border="1">--%>
<%--    <thead>--%>
<%--    <tr>--%>
<%--        <th>Название устройства</th>--%>
<%--        <th>Ip:port</th>--%>
<%--        <th>Статус</th>--%>
<%--        <th>Состояние</th>--%>
<%--        <th>UserId</th>--%>
<%--        <th>Type</th>--%>
<%--        <th>Do</th>--%>
<%--&lt;%&ndash;        <th>Действия(просмотр, ред., удаление)</th>&ndash;%&gt;--%>
<%--    </tr>--%>
<%--    </thead>--%>
<%--    <tbody>--%>
<%--    <tr>--%>
<%--        <th><c:out value="${Dev.aliasName}" /></th>--%>
<%--        <th><c:out value="${Dev.ip}" />:<c:out value="${Dev.port}" /></th>--%>
<%--        <th><c:out value="${Dev.status}" /></th>--%>
<%--        <th><c:out value="${Dev.operating}" /></th>--%>
<%--        <th><c:out value="${Dev.userId}" /></th>--%>
<%--        <th><c:out value="${Dev.getMyTypes()}" />--%>
<%--        </th>--%>

<%--        <th><form name="frm" method="get" action="http://localhost:8762/api/device/devices/do-command/?id=${DevId}&commandId=1">--%>
<%--            <input type="button" name="bt" value="on" onclick="{submit();}" />--%>
<%--        </form> / <form name="frm1" method="get" action="http://localhost:8762/api/device/devices/do-command/?id=${DevId}&commandId=0">--%>
<%--            <input type="hidden" name="hdnbt" />--%>
<%--            <input type="button" name="bt1" value="off" onclick="{document.frm1.hdnbt.value=this.value;document.frm1.submit();}" />--%>
<%--        </form> </th>--%>
<%--    </tr>--%>
<%--    </tbody>--%>
<%--</table>--%>

<h1>devices</h1>
<table class="table" border="1">
    <thead>
    <tr>
        <th>Название устройства</th>
        <th>Ip:port</th>
        <th>Статус</th>
        <th>Состояние</th>
        <th>UserId</th>
        <th>Type</th>
        <th>Do</th>
        <th>Действия(просмотр, ред., удаление)</th>
    </tr>
    </thead>
    <tbody>
        <c:forEach var="item" items="${Devs}">
            <tr>
                <td scope="row">${item.aliasName}</td>
                <td>${item.ip}:${item.port}</td>
                <td>${item.status}</td>
                <td>${item.operating}</td>
                <td>${item.userId}</td>
                <td>${item.getMyTypes()}</td>
                <th>
                    <a href="http://localhost:8762/api/device/devices/do-command/?id=${item.id}&commandId=1">on</a>
                    <a href="http://localhost:8762/api/device/devices/do-command/?id=${item.id}&commandId=0">off</a>

                </th>
                <td>
                    <a href="${pageContext.request.contextPath}/web/deviceView/?id=${item.id}">View</a>
                    <a href="${pageContext.request.contextPath}/web/deviceEdit/?id=${item.id}">Edit</a>
                    <a href="http://localhost:8762/api/device/devices/device/delete/?id=${item.id}">Delete</a>
<%--                    <a href="${pageContext.request.contextPath}/showdevice?id=${device.id}">Link</a>                            &nbsp;&nbsp;&nbsp;&nbsp;--%>
<%--                    <a href="${pageContext.request.contextPath}/editdevice?id=${device.id}">Edit</a>                            &nbsp;&nbsp;&nbsp;&nbsp;--%>
<%--                    <a href="${pageContext.request.contextPath}/deletedevice?id=${device.id}">Delete</a>--%>
                </td>
            </tr>
        </c:forEach>
    </tbody>
</table>
</body>
</html>