<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<jsp:useBean id="Dev" class="dto.deviceservice.entities.DeviceDto" scope="request"/>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="resources/css/style.css">
    <script type="text/javascript" src="/resources/js/app.js"></script>
    <title>SmartHome</title>
</head>
<body>

<jsp:include page="top1.jsp"></jsp:include>

<h1>device edit page</h1>
<table class="table" border="1">
    <thead>
    <tr>
        <th>Название устройства</th>
        <th>Ip:port</th>
        <th>Статус</th>
        <th>Состояние</th>
        <th>UserId</th>
        <th>Type</th>
<%--        <th>Do</th>--%>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th><c:out value="${Dev.aliasName}" /></th>
        <th><c:out value="${Dev.ip}" />:<c:out value="${Dev.port}" /></th>
        <th><c:out value="${Dev.status}" /></th>
        <th><c:out value="${Dev.operating}" /></th>
        <th><c:out value="${Dev.userId}" /></th>
        <th><c:out value="${Dev.getMyTypes()}" /></th>
    </tr>
    </tbody>
</table>