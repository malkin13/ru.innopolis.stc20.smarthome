<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<jsp:useBean id="Scenario" class="dto.scenarioservice.entities.ScenarioDto" scope="request"/>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="resources/css/style.css">
    <script type="text/javascript" src="/resources/js/app.js"></script>
    <title>SmartHome</title>
</head>
<body>

<jsp:include page="top1.jsp"></jsp:include>

<h1>scenario view page</h1>

<table class="table" border="1">
    <thead>
    <tr>
        <th>Название сценария</th>
        <th>Устройство</th>
        <th>Комманда</th>
        <th>Статус</th>
        <th>Время выполнения</th>
        <th>UserId</th>
<%--        <th>Действия(просмотр, ред., удаление)</th>--%>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th><c:out value="${Scenario.aliasName}" /></th>
        <th><c:out value="${Scenario.deviceId}" /></th>
        <th><c:out value="${Scenario.commandId}" /></th>
        <th><c:out value="${Scenario.status}" /></th>
        <th><c:out value="${Scenario.runTime}" /></th>
        <th><c:out value="${Scenario.userId}" />
    </tr>
    </tbody>
</table>