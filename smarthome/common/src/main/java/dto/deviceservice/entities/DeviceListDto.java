package dto.deviceservice.entities;

import dto.base.BaseEntityDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;


@EqualsAndHashCode(callSuper = true)
@Data
public class DeviceListDto extends ArrayList<DeviceDto> {

}
