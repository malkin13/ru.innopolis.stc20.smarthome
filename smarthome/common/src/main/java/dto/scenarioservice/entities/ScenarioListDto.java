package dto.scenarioservice.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;

@EqualsAndHashCode(callSuper = true)
@Data
public class ScenarioListDto extends ArrayList<ScenarioDto> {
}
